require 'socket'

class RubySam::SongRequester
  class RequestError < StandardError; end

  def self.request!(song, params = {})
    socket = TCPSocket.new(RubySam.sam_host, RubySam.sam_port)
    request_str = "GET /req/?songID=#{song.id}&host=#{RubySam.host} HTTP/1.0\r\n\r\n"
    socket.send(request_str, 0)

    line = ""
    while (line != "\r\n") do
      line = socket.gets(128)
    end

    xml_data = socket.read()
    socket.close()

    data = Hash.from_xml(xml_data)
    request_data = data["REQUEST"]
    code = request_data['status']['code']

    if params[:name]
      params[:name] = params[:name].encode(RubySam::QUERY_ENCODING)
    end
    if params[:msg]
      params[:msg] = params[:msg].encode(RubySam::QUERY_ENCODING)
    end

    if code == '200'
      request = RubySam::Models::Request.find(request_data['status']['requestID'])
      request.update_attributes(params)
      request
    else
      raise RequestError.new(request_data['status']['message'])
    end
  end
end
