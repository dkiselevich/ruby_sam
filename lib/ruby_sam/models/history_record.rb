module RubySam::Models
  class HistoryRecord < Base
    self.table_name = 'historylist'

    belongs_to :song, foreign_key: 'songID'
    belongs_to :request, foreign_key: 'requestID'

    def self.recent
      order("#{HistoryRecord.table_name}.date_played DESC")
    end

    def self.current
      recent.includes(:song, :request).first
    end

  end
end
