module RubySam::Models
  class QueueRecord < Base
    self.table_name = 'queuelist'

    belongs_to :song, foreign_key: 'songID'
    belongs_to :request, foreign_key: 'requestID'
  end
end
