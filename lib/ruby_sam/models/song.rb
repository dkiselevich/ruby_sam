module RubySam::Models
  class Song < Base
    SONGTYPES = {
      normal: 'S',
      station_id: 'I',
      promo: 'P',
      jingle: 'J',
      advertisement: 'A',
      syndicated_news: 'N',
      interviews: 'V',
      sound_fx: 'X',
      unknown_content: 'C',
      unknown: '?'
    }

    self.table_name = 'songlist'

    has_many :queue_records, foreign_key: 'songID'
    has_many :history_records, foreign_key: 'songID'
    has_many :requests, foreign_key: 'songID'

    SONGTYPES.each do |k, v|
      define_singleton_method k do
        where('songtype = ?', v)
      end
    end

    def self.queued
      joins(:queue_records).order("#{QueueRecord.table_name}.sortID ASC")
    end

    def self.comming
      normal.queued
    end

    def self.recent
      joins(:history_records).order("#{HistoryRecord.table_name}.date_played DESC")
    end

    def self.current
      self.recent.first
    end

    def self.top_requested
      joins(:requests).where(requests: { code: 200 }).order("#{Song.table_name}.requested_count DESC")
    end

    def self.with_artists(regexp = nil)
      regexp_string = regexp.respond_to?(:source) ? regexp.source : regexp
      where("artist REGEXP ?", regexp_string.encode(RubySam::QUERY_ENCODING))
    end

    def self.artists(regexp = nil)
      query = select('distinct artist')
      if regexp
        query = query.with_artists(regexp)
      end
      query.map(&:artist)
    end

    def self.search(string = nil)
      query = self
      unless string.empty?
        words = string.encode(RubySam::QUERY_ENCODING).split(/[\,\.\-\+\s]/).reject {|w| w.empty?}
        if words.any?
          like_words = words.map {|word| "%#{word}%" }

          where_string = (["title like ?"] * like_words.size).join(' OR ') + ' OR '
          where_string += (["artist like ?"] * like_words.size).join(' OR ') + ' OR '
          where_string += (["album like ?"] * like_words.size).join(' OR ')
          query = query.where(where_string, *(like_words * 3))
        end
      end
      query
    end
  end
end
