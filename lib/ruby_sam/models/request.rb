module RubySam::Models
  class Request < Base
    self.table_name = 'requestlist'

    attr_accessible :msg, :name
    belongs_to :song, foreign_key: 'songID'

    has_many :queue_records, foreign_key: 'requestID'
    has_many :history_records, foreign_key: 'requestID'

  end
end
