module RubySam::Models
  autoload :Base, 'ruby_sam/models/base'
  autoload :Song, 'ruby_sam/models/song'
  autoload :QueueRecord, 'ruby_sam/models/queue_record'
  autoload :HistoryRecord, 'ruby_sam/models/history_record'
  autoload :Request, 'ruby_sam/models/request'
end
