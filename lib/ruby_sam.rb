require "active_record"
require "ruby_sam/version"
require "ruby_sam/models"
require "ruby_sam/song_requester"

#RubySam::Models::Base.establish_connection({
#    adapter: 'mysql',
#    host: '192.168.0.106',
#    port: 3306,
#    username: 'root',
#    database: 'SAMDB',
#    encoding: 'latin1'
#  })
#  #establish_connection('mysql://radio@204.140.27.122:3306/SAMDB')

module RubySam
  QUERY_ENCODING = 'cp1251'
  # Your code goes here...
  class << self
    attr_accessor :connection
    attr_accessor :sam_host
    attr_accessor :sam_port
    attr_accessor :host

    def config
      yield self
      connection[:reconnect] = true
      RubySam::Models::Base.establish_connection(connection)
    end
  end
end

#RubySam.config do |c|
#  c.connection = {
#    adapter: 'mysql',
#    host: '192.168.0.106',
#    port: 3306,
#    username: 'root',
#    database: 'SAMDB',
#    encoding: 'latin1'
#  }
#  c.sam_host = '192.168.56.100'
#  c.sam_port= 1221
#  c.host = '192.168.56.1'
#end
