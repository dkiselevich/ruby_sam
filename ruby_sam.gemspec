# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ruby_sam/version'

Gem::Specification.new do |gem|
  gem.name          = "ruby_sam"
  gem.version       = RubySam::VERSION
  gem.authors       = ["Dmitriy Dudkin"]
  gem.email         = ["dudkin.dmitriy@gmail.com"]
  gem.description   = %q{SAM Broadcaster ruby integration}
  gem.summary       = %q{}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'bundler'
  gem.add_dependency "activerecord", "~> 3.2.10"
  gem.add_dependency "mysql2"
  gem.add_dependency "squeel"
end
